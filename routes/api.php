<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(App\Http\Controllers\AuthController::class)->group(function () {
    Route::post('/mobile/register', 'register');
    Route::post('/mobile/login', 'login');
    Route::get('/mobile/logout', 'logout')->middleware('auth:mobile');
});
Route::controller(App\Http\Controllers\ForgotPasswordController::class)->group(function () {

    Route::post('/user/password/email', 'userForgotPassword');

    Route::post('user/password/code/check',  'userCheckCode');

    Route::post('/user/password/reset', 'userResetPassword');

});


// Event categories
Route::get('/event-categories', [MainController::class, 'eventCategories']);

// Service categories
Route::get('/service-categories', [MainController::class, 'serviceCategory']);

// Service providers by category
Route::get('/service-providers/{id}', [MainController::class, 'serviceProvider']);

// Venues
Route::get('/venues', [MainController::class, 'venue']);

// Reels
Route::get('/reels', [MainController::class, 'reel']);

// User's event requests
Route::get('/my-events', [MainController::class, 'myEventRequest'])->middleware('auth:mobile');  // Ensure you have a proper auth middleware

// Request to create an event
Route::post('/create-events', [MainController::class, 'requestEvent'])->middleware('auth:mobile');
