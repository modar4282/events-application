<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'event_category_id', 'description', 'people_number', 'venue_id', 'type', 'date' , 'mobile_user_id'];

    protected $with = ['eventCategory', 'venue', 'serviceProviders', 'timelines'];

    public function eventCategory()
    {
        return $this->belongsTo(EventCategory::class);
    }

    public function venue()
    {
        return $this->belongsTo(Venue::class);
    }

    public function serviceProviders()
    {
        return $this->belongsToMany(ServiceProvider::class, 'event_service_provider', 'event_id', 'service_provider_id');
    }

    public function timelines()
    {
        return $this->hasMany(EventTimeline::class);
    }
}
