<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'location', 'images', 'capacity', 'price', 'description'];

    public function events()
    {
        return $this->hasMany(Event::class);
    }
    protected $casts = [
        'images' => 'array',
    ];
}
