<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProvider extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'images', 'videos', 'service_category_id', 'wage'];

    public function serviceCategory()
    {
        return $this->belongsTo(ServiceCategory::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_service_provider', 'service_provider_id', 'event_id');
    }
    protected $casts = [
        'images' => 'array',
        'videos' => 'array',
    ];
}
