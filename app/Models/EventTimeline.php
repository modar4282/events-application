<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventTimeline extends Model
{
    use HasFactory;
    protected $fillable= ['event_id' , 'start_time' , 'end_time' , 'description'] ;

    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id') ;
    }
}
