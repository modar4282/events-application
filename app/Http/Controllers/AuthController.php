<?php

namespace App\Http\Controllers;

use App\Models\MobileUser;
use App\Traits\FileStorageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use FileStorageTrait ;
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:mobile_users',
            'password' => 'required|string|min:8',
            'birthdate' => 'required|date',
            'governorate' => 'required|string|max:255',
            'profile_image' => 'nullable|image'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $path = null ;
        if ($request['profile_image'])
            $path = $this->storefile($request->file('profile_image'), 'user/images');


        $user = MobileUser::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'birthdate' => $request->birthdate,
            'governorate' => $request->governorate,
            'profile_image' => $path
        ]);

        $token = $user->createToken('mobileToken')->plainTextToken;

        return response()->json(['user' => $user, 'token' => $token], 201);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Manually retrieve the user by email
        $user = MobileUser::where('email', $request->email)->first();

        // Check if user exists and password is correct
        if ($user && Hash::check($request->password, $user->password)) {
            $token = $user->createToken('mobileToken')->plainTextToken;
            return response()->json(['user' => $user, 'token' => $token]);
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }


    public function logout(Request $request)
    {
        $request->user('mobile')->currentAccessToken()->delete();
        return response()->json(['message' => 'Logged out']);
    }

}
