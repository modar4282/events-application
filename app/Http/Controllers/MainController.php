<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventCategory;
use App\Models\Reel;
use App\Models\ServiceCategory;
use App\Models\ServiceProvider;
use App\Models\Venue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function eventCategories()
    {
        $eventCategory = EventCategory::all() ;

        return response()->json([
            'status' => true ,
            'data' => $eventCategory
        ]) ;
    }

    public function serviceCategory()
    {
        $serviceCategory = ServiceCategory::all() ;
        return response()->json([
            'status' => true ,
            'data' => $serviceCategory
        ]) ;
    }

    public function serviceProvider($id)
    {
        $serviceCategory = ServiceProvider::where('service_category_id' , $id)->with('serviceCategory')->get() ;
        return response()->json([
            'status' => true ,
            'data' => $serviceCategory
        ]) ;
    }

    public function venue()
    {
        $venue = Venue::all() ;

        return response()->json([
            'status' => true ,
            'data' => $venue
        ]);
    }

    public function reel()
    {
        $reel = Reel::all() ;

        return response()->json([
            'status' => true ,
            'data' => $reel
        ]);
    }

    public function myEventRequest()
    {
        $request = Event::with(['timelines' , 'serviceProviders' , 'venue' , 'eventCategory' ])->where('mobile_user_id' , Auth::guard('mobile')->id())->get() ;
        return response()->json([
            'status' => true ,
            'data' => $request
        ]) ;
    }


    public function requestEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'event_category_id' => 'required|integer|exists:event_categories,id',
            'description' => 'required|string',
            'people_number' => 'required|integer',
            'venue_id' => 'required|integer|exists:venues,id',
            'service_provider_ids' => 'required|array',
            'service_provider_ids.*' => 'integer|exists:service_providers,id',
            'date' => 'required|date',
            'type' => 'required|in:pending,confirmed,cancelled',
            'timelines' => 'required|array',
            'timelines.*.start_time' => 'required|date_format:H:i',
            'timelines.*.end_time' => 'required|date_format:H:i|after:timelines.*.start_time',
            'timelines.*.description' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        // Create the event
        $event = new Event;
        $event->title = $request->title;
        $event->event_category_id = $request->event_category_id;
        $event->description = $request->description;
        $event->people_number = $request->people_number;
        $event->venue_id = $request->venue_id;
        $event->date = $request->date;
        $event->type = $request->type;
        $event->mobile_user_id = Auth::guard('mobile')->id();
        $event->save();
        $event->serviceProviders()->attach($request->service_provider_ids);
        // Create timelines associated with the event
        foreach ($request->timelines as $timeline) {
            $event->timelines()->create([
                'start_time' => $timeline['start_time'],
                'end_time' => $timeline['end_time'],
                'description' => $timeline['description']
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Event created successfully with timelines',
            'data' => $event->load('timelines')  // Eager load timelines to include in the response
        ]);
    }


}
