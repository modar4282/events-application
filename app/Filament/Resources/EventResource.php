<?php

namespace App\Filament\Resources;

use App\Filament\Resources\EventResource\Pages;
use App\Models\Event;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class EventResource extends Resource
{
    protected static ?string $model = Event::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form->schema([
            Forms\Components\TextInput::make('title')
                ->required()
                ->disabled(),

            Forms\Components\BelongsToSelect::make('event_category_id')
                ->relationship('eventCategory', 'name')
                ->required()
                ->disabled(),

            Forms\Components\Textarea::make('description')
                ->required()
                ->columnSpan(2)
                ->rows(3)
                ->disabled(),

            Forms\Components\TextInput::make('people_number')
                ->numeric()
                ->required()
                ->disabled(),

            Forms\Components\BelongsToSelect::make('venue_id')
                ->relationship('venue', 'name')
                ->required()
                ->disabled(),


            Forms\Components\DatePicker::make('date')
                ->required()
                ->columnSpan(2)
                ->disabled(),

            Forms\Components\HasManyRepeater::make('timelines')
                ->relationship('timelines')
                ->schema([
                    Forms\Components\TimePicker::make('start_time')->required()->disabled(),
                    Forms\Components\TimePicker::make('end_time')->required()->disabled(),
                    Forms\Components\Textarea::make('description')->required()->disabled(),
                ])->columnSpan(2),

            Forms\Components\BelongsToManyMultiSelect::make('service_provider_ids')
                ->relationship('serviceProviders', 'name')
                ->getOptionLabelFromRecordUsing(function ($record) {
                    return "{$record->name} ({$record->wage}) SP";
                })
                ->required()
                ->columnSpan(2)
                ->disabled(),

            Forms\Components\Select::make('type')
                ->options([
                    'pending' => 'Pending',
                    'confirmed' => 'Confirmed',
                    'cancelled' => 'Cancelled',
                ])
                ->required()
                ->columnSpan(2)
                ->default('pending')  // Ensure that 'pending' is the default selection
                ->disabled(false),  // Make it editable if required
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            Tables\Columns\TextColumn::make('title'),
            Tables\Columns\TextColumn::make('eventCategory.name')->label('Category'),
            Tables\Columns\TextColumn::make('venue.name')->label('Venue'),
            Tables\Columns\TextColumn::make('venue.price')->label('Venue Price'),
            Tables\Columns\TextColumn::make('type'),
            Tables\Columns\TextColumn::make('date')->date(),
        ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListEvents::route('/'),
            'create' => Pages\CreateEvent::route('/create'),
            'edit' => Pages\EditEvent::route('/{record}/edit'),
            ];
    }

    public static function getNavigationGroup(): ?string
    {
        return 'Events' ;
    }

    public static function getNavigationSort(): ?int
    {
        return 1;
    }
}
